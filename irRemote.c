#include "simpletools.h"                      // Libraries included
#include "sirc.h"

int main()                                    // main function
{
  sirc_setTimeout(1000);                      // -1 if no remote code in 1 s

  while(1)                                    // Repeat indefinitely
  {                                         
                                              //  decoding signal from P10
 //   print("%c Remote Button = %d%c",          //  Display button # decoded
 //         HOME, sirc_button(10), CLREOL);     //  from signal on P10
  
    int button = sirc_button(0);
    
    switch(button){ 
      
      case 21 : 
        print("%c Remote Button = TV Power%c",HOME, CLREOL);
        break;
      case 18 : 
          print("%c Remote Button = Volume +%c",HOME, CLREOL);
          break;
      case 19 : 
          print("%c Remote Button = Volume -%c",HOME, CLREOL);
          break;    
      case 16 : 
          print("%c Remote Button = Channel +%c",HOME, CLREOL);
          break;    
      case 17 : 
          print("%c Remote Button = Channel -%c",HOME, CLREOL);
          break;
      case 20 : 
          print("%c Remote Button = Mute%c",HOME, CLREOL);
          break; 
      case 37 : 
          print("%c Remote Button = Input%c",HOME, CLREOL);
          break;    
      case 54 : 
          print("%c Remote Button = Asterisk(*)%c",HOME, CLREOL);
          break;    
      case 96 : 
          print("%c Remote Button = Menu%c",HOME, CLREOL);
          break;    
      case 101 : 
          print("%c Remote Button = OK%c",HOME, CLREOL);
          break;
      case 116 : 
          print("%c Remote Button = D-Pad Up%c",HOME, CLREOL);
          break;  
      case 117 : 
          print("%c Remote Button = D-Pad Down%c",HOME, CLREOL);
          break;  
      case 52 : 
          print("%c Remote Button = D-Pad Left%c",HOME, CLREOL);
          break;
      case 51 : 
          print("%c Remote Button = D-Pad Right%c",HOME, CLREOL);
          break;    
      case 27 : 
          print("%c Remote Button = Reverse%c",HOME, CLREOL);
          break;                
      case 26 : 
          print("%c Remote Button = Play%c",HOME, CLREOL);
          break;
      case 25 : 
          print("%c Remote Button = Pause%c",HOME, CLREOL);
          break;
      case 28 : 
          print("%c Remote Button = Fast Forward%c",HOME, CLREOL);
          break;
      case 32 : 
          print("%c Remote Button = Record%c",HOME, CLREOL);
          break;
      case 24 : 
          print("%c Remote Button = Stop%c",HOME, CLREOL);
          break;
      case 0 : 
          print("%c Remote Button = Num Pad 0%c",HOME, CLREOL);
          break;
      case 1 : 
          print("%c Remote Button = Num Pad 1%c",HOME, CLREOL);
          break;
      case 2 : 
          print("%c Remote Button = Num Pad 2%c",HOME, CLREOL);
          break;
      case 3 : 
          print("%c Remote Button = Num Pad 3%c",HOME, CLREOL);
          break;
      case 4 : 
          print("%c Remote Button = Num Pad 4%c",HOME, CLREOL);
          break;
      case 5 : 
          print("%c Remote Button = Num Pad 5%c",HOME, CLREOL);
          break;
      case 6 : 
          print("%c Remote Button = Num Pad 6%c",HOME, CLREOL);
          break;  
      case 7 : 
          print("%c Remote Button = Num Pad 7%c",HOME, CLREOL);
          break;
      case 8 : 
          print("%c Remote Button = Num Pad 8%c",HOME, CLREOL);
          break;
      case 9 : 
          print("%c Remote Button = Num Pad 9%c",HOME, CLREOL);
          break;    
    }            
    pause(100);                               // 1/10 s before loop repeat
  }  
}