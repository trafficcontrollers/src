#include "simpletools.h"                      // Include simpletools library
#include "adcDCpropab.h"                      // Include adcDCpropab library


int milliseconds;
int msTicks;
int tSync;

void background();

int main()
{
  float ad0, ad1;
  float cm = 40;
  float time1, time2;
  adc_init(21, 20, 19, 18);                   // Initialize ADC on Activity Board
  
  milliseconds = 0;
  cog_run(background, 128);

  while(1)                                    // Loop repeats indefinitely
  {
    ad0 = adc_volts(0);                      
    ad1 = adc_volts(1);
    
    putChar(HOME);                            // Set cursor to top left
    print("Reading: %f%c\n", ad0, CLREOL);    // Display voltage from sensor
    print("Reading: %f%c\n", ad1, CLREOL);
   
    if(ad0 > 2 & ad1 < 3){
        time1 = milliseconds;
    }
    if(ad1 > 2 & ad1 < 3){
        time2 = milliseconds;
    }  
    print("Current time (ms): %d %c\n", milliseconds,CLREOL); 
    print("Time1 (ms): %d %c\n", time1,CLREOL);
    print("Time2 (ms): %d %c\n", time2,CLREOL);  
    print("Speed (cm/s): %f%c\n", 1000*(cm/(time2-time1)), CLREOL);
    
    //pause(200);
    
    }
     
                           
 
}  
  void background(){
  msTicks = ((CLKFREQ) / 1000);
  tSync = (CNT);
  while (1) {
    tSync = (tSync + msTicks);
    waitcnt(tSync);
    milliseconds = (milliseconds + 1);
  }
  }