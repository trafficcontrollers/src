#include <propeller.h>
#include "ws2812.h"

// led chain
#define LED_PIN     13
#define LED_COUNT   6

// 4 Parallax WS2812B Fun Boards
uint32_t ledColors[LED_COUNT];

// LED driver state
ws2812 *driver;

// pattern for chase
uint32_t pattern[] = {
    0x160000,
    0x161600, 
    0x001600
};

#define pattern_count  (sizeof(pattern) / sizeof(pattern[0]))

// ticks per millisecond
int ticks_per_ms;

// forward declarations
void chase(int count, int delay);
void NSpassTranstoStop(void);
void pause(int ms);

void main(void)
{
    // calibrate the pause function
    // CLKFREQ is the clock frequency of the processor
    // typically this is 80mhz
    // dividing by 1000 gives the number of clock ticks per millisecond
    ticks_per_ms = CLKFREQ / 100;
    
    // load the LED driver
    if (!(driver = ws2812b_open()))
        return;
        
    // repeat the patterns
    for (;;) {
    
        NSpassTranstoStop();
        
      
    }
    
    // close the driver
    ws2812_close(driver);
}

void chase(int count, int delay)
{
    int base = 0;
    int idx, i;
    
    // repeat count times or forever if count < 0
    while (count < 0 || --count >= 0) {
    
        // fill the chain with the pattern
        idx = base;                             // start at base
        for (i = 0; i < LED_COUNT; ++i) {       // loop through connected leds
            ledColors[i] = pattern[idx];        // Set channel color
            if (++idx >= pattern_count)              // past end of list?
                idx = 0;                        // yes, reset
        }
        if (++base >= pattern_count)            // Set the base for the next time
            base = 0;
    
        // Set LEDs in the chain
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
            
        // delay between frames
        pause(delay);
    }
}

void NSpassTranstoStop(void){
    
        ledColors[0] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[1] = 0;         //NS LIGHTS
        ledColors[2] = 0x001600;
       
        ledColors[3] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[4] = 0;         //WE LIGHTS
        ledColors[5] = 0;       
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(1000);
        
        ledColors[0] = 0;        
        ledColors[1] = 0x161600;   //YELLOW LOW BRIGHTNESS
        ledColors[2] = 0;
        
        ledColors[3] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0;
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(1000);
        
        ledColors[0] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0;
        
        ledColors[3] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0x001600;
       
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(1000);
        
        WEpassTranstoStop();
        
        
} 

void WEpassTranstoStop(void){
    
        ledColors[0] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[1] = 0;         //NS LIGHTS
        ledColors[2] = 0;
       
        ledColors[3] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0;         //WE LIGHTS
        ledColors[5] = 0x001600;       
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(1000);
        
        ledColors[0] = 0x160000;  //YELLOW LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0;
        
        ledColors[3] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0x161600;
        ledColors[5] = 0;
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);
        
        ledColors[0] = 0;  //RED LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0x001600;
        
        ledColors[3] = 0x160000;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0;
       
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(1000);
        
        
        
}                           

void pause(int ms)
{
    waitcnt(CNT + ms * ticks_per_ms);
}