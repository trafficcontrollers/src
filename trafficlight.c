#include <propeller.h>    
#include "simpletools.h"            
#include "ws2812.h"                           // RGB LED Header File
#include "adcDCpropab.h"                      // Analog to Digital header
#include "sirc.h"                             // IR Remote header

#define LED_PIN     13                        // Assign pin 13
#define LED_COUNT   6                         // # of ws2812bs

/************************* Global Variables **********************/
uint32_t ledColors[LED_COUNT];
ws2812 *driver;
int ticks_per_ms;
int pin;
float ad1, ad2;
int time1, time2;
float cm = 40;

int milliseconds;                             //Next 3 lines used for background timer
int msTicks;
int tSync;

int *timer, 
    *lights, 
    *remote,
    *snsr;
    
#define pattern_count  (sizeof(pattern) / sizeof(pattern[0]))
uint32_t pattern[] = {                       // pattern for chase
    0x160000,
    0x161600, 
    0x001600
};

/********************** End Global Variables *********************/

/********************* Function Declarations *********************/
void NSpassTranstoStop();
void WEpassTranstoStop();
void pause(int ms);
void traffic();
void irRemote();
void background();
void allStop();
void lightsoff();
void speedSens();
void chase(int count, int delay);

/******************** End Function Declarations ******************/

int main(){
  adc_init(21, 20, 19, 18);                   // Initialize ADC on Activity Board
  milliseconds = 0;                           // initialize milliseconds
  /* Calibrates pause function 
   * CLKFREQ = processor clk frq ~80 MHz
   * divide 1000 = clk ticks per millisecond
   */
  ticks_per_ms = CLKFREQ / 100;              // Set to 100 to slow down lights
  if (!(driver = ws2812b_open()))             // Loads LED Driver
        return 1;
   
  simpleterm_close();                       // Close SimpleIDE Terminal for this core
  timer = cog_run(background, 128);           // cog running timer
  lights = cog_run(traffic, 128);             // cog running the lights            
  remote = cog_run(irRemote, 128);            // cog running ir remote
  snsr = cog_run(speedSens, 128);                 // cog reading voltages
  
 
}

/******************** Function Definitions ***********************/

void traffic(){
  while(1){
   
    NSpassTranstoStop();
    if(pin > 1){
      pause(2000);
    }
    WEpassTranstoStop();
  }   
}  

void allStop(){
        ledColors[0] = 0x160000;  // NS RED LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0;
        
        ledColors[3] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0;
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
}  

void lightsoff(){
        ledColors[0] = 0;  // NS RED LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0;
        
        ledColors[3] = 0;  //RED LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0;
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
}  

void NSpassTranstoStop(){
    
        ledColors[0] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[1] = 0;         //NS LIGHTS
        ledColors[2] = 0x001600;
       
        ledColors[3] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[4] = 0;         //WE LIGHTS
        ledColors[5] = 0;       
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);
        
        ledColors[0] = 0;        
        ledColors[1] = 0x161600;   //YELLOW LOW BRIGHTNESS
        ledColors[2] = 0;
        
        ledColors[3] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0;
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);
        
        ledColors[0] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0;
        
        ledColors[3] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0x001600;
       
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);
     
} 

void WEpassTranstoStop(){
    
        ledColors[0] = 0x160000;  //RED LOW BRIGHTNESS
        ledColors[1] = 0;         //NS LIGHTS
        ledColors[2] = 0;
       
        ledColors[3] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0;         //WE LIGHTS
        ledColors[5] = 0x001600;       
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);
        
        ledColors[0] = 0x160000;  //YELLOW LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0;
        
        ledColors[3] = 0;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0x161600;
        ledColors[5] = 0;
        
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);
        
        ledColors[0] = 0;  //RED LOW BRIGHTNESS
        ledColors[1] = 0;
        ledColors[2] = 0x001600;
        
        ledColors[3] = 0x160000;         //GREEN LOW BRIGHTNESS
        ledColors[4] = 0;
        ledColors[5] = 0;
       
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
        pause(500);    
}                           

void pause(int ms){
    waitcnt(CNT + ms * ticks_per_ms);
}


void irRemote(){
  sirc_setTimeout(1000);                      // -1 if no remote code in 1 s
  while(1){  
   
  int button = sirc_button(0);                // connected to pin 0
    switch(button){
      
      case 21 : 
        print("%c Remote Button = TV Power%c",HOME, CLREOL);
        cog_end(lights); 
        chase(50, 20);
        break;
      case 18 : 
          print("%c Remote Button = Volume +%c",HOME, CLREOL);
          break;
      case 19 : 
          print("%c Remote Button = Volume -%c",HOME, CLREOL);
          break;    
      case 16 : 
          print("%c Remote Button = Channel +%c",HOME, CLREOL);
          break;    
      case 17 : 
          print("%c Remote Button = Channel -%c",HOME, CLREOL);
          break;
      case 20 : 
          print("%c Remote Button = Mute%c",HOME, CLREOL);
          break; 
      case 37 : 
          print("%c Remote Button = Input%c",HOME, CLREOL);
          break;    
      case 54 : 
          print("%c Remote Button = Asterisk(*)%c",HOME, CLREOL);
          break;    
      case 96 : 
          print("%c Remote Button = Menu%c",HOME, CLREOL);
          break;    
      case 101 : 
          print("%c Remote Button = OK%c",HOME, CLREOL);
          break;
      case 116 : 
          print("%c Remote Button = D-Pad Up%c",HOME, CLREOL);
          break;  
      case 117 : 
          print("%c Remote Button = D-Pad Down%c",HOME, CLREOL);
          break;  
      case 52 : 
          print("%c Remote Button = D-Pad Left%c",HOME, CLREOL);
          break;
      case 51 : 
          print("%c Remote Button = D-Pad Right%c",HOME, CLREOL);
          break;    
      case 27 : 
          print("%c Remote Button = Reverse%c",HOME, CLREOL);
          break;                
      case 26 : 
          print("%c Remote Button = Play%c",HOME, CLREOL);
          lights = cog_run(traffic, 128);               // cog running the lights  
          break;
      case 25 : 
          print("%c Remote Button = Pause%c",HOME, CLREOL);
          break;
      case 28 : 
          print("%c Remote Button = Fast Forward%c",HOME, CLREOL);
          break;
      case 32 : 
          print("%c Remote Button = Record%c",HOME, CLREOL);
          break;
      case 24 : 
          print("%c Remote Button = Stop%c",HOME, CLREOL);
          cog_end(lights); 
          lightsoff();
          break;
      case 0 : 
          print("%c Remote Button = Num Pad 0%c",HOME, CLREOL);
          cog_end(lights);
          allStop();
          break;
      case 1 : 
          print("%c Remote Button = Num Pad 1%c",HOME, CLREOL);
         
          break;
      case 2 : 
          print("%c Remote Button = Num Pad 2%c",HOME, CLREOL);
       
          break;
      case 3 : 
          print("%c Remote Button = Num Pad 3%c",HOME, CLREOL);
          break;
      case 4 : 
          print("%c Remote Button = Num Pad 4%c",HOME, CLREOL);

          break;
      case 5 : 
          print("%c Remote Button = Num Pad 5%c",HOME, CLREOL);

          break;
      case 6 : 
          print("%c Remote Button = Num Pad 6%c",HOME, CLREOL);
          break;  
      case 7 : 
          print("%c Remote Button = Num Pad 7%c",HOME, CLREOL);
          break;
      case 8 : 
          print("%c Remote Button = Num Pad 8%c",HOME, CLREOL);
          break;
      case 9 : 
          print("%c Remote Button = Num Pad 9%c",HOME, CLREOL);
          break;    
    }            
    pause(100);                               // 1/10 s before loop repeat
  } 
  
}  

void background(){
  msTicks = ((CLKFREQ) / 1000);
  tSync = (CNT);
  while (1) {
    tSync = (tSync + msTicks);
    waitcnt(tSync);
    milliseconds = (milliseconds + 1);
  }
}  

void speedSens(){
  simpleterm_open();                        // Open SimpleIDE Terminal for this core
  while(1){
    pin = adc_volts(0);                     // Assign adc 0 to pin
    ad1 = adc_volts(1);                      
    ad2 = adc_volts(2);
    
    putChar(HOME);                            // Set cursor to top left
    print("IR Sens 1: %f%c\n", ad1, CLREOL);    // Display voltage from sensor
    print("IR Sens 2: %f%c\n", ad2, CLREOL);
   
    if(ad1 > 2 & ad1 < 3){
        time1 = milliseconds;
    }
    if(ad2 > 2 & ad2 < 3){
        time2 = milliseconds;
    }  
    
    print("Current time (ms): %d %c\n", milliseconds, CLREOL); 
    print("Time1 (s): %d %c\n", 1000*time1, CLREOL);
    print("Time2 (s): %d %c\n", 1000*time2, CLREOL);  
    print("Speed (cm/s): %f%c\n", 1000*(cm/(time2-time1)), CLREOL); // x1000 for ms to s conversion
    
   }      
   
   simpleterm_close();                       // Close SimpleIDE Terminal 
}  

void chase(int count, int delay)
{
    int base = 0;
    int idx, i;
    
    // repeat count times or forever if count < 0
    while (count < 0 || --count >= 0) {
    
        // fill the chain with the pattern
        idx = base;                             // start at base
        for (i = 0; i < LED_COUNT; ++i) {       // loop through connected leds
            ledColors[i] = pattern[idx];        // Set channel color
            if (++idx >= pattern_count)              // past end of list?
                idx = 0;                        // yes, reset
        }
        if (++base >= pattern_count)            // Set the base for the next time
            base = 0;
    
        // Set LEDs in the chain
        ws2812_set(driver, LED_PIN, ledColors, LED_COUNT);
            
        // delay between frames
        pause(delay);
    }
} 